﻿
using Microsoft.Net.Http.Headers;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
#if DOTNET2
using Microsoft.AspNetCore.Mvc.Formatters;
#else
using Microsoft.AspNet.Mvc.Formatters;
#endif


namespace LendFoundry.WebhookNotifier.Api.Controllers
{
    internal class CustomInputFormatter : TextInputFormatter
    {
        public CustomInputFormatter()
        {
            SupportedEncodings.Add(UTF8EncodingWithoutBOM);
            SupportedEncodings.Add(UTF16EncodingLittleEndian);

            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/xml").CopyAsReadOnly());
            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("text/xml").CopyAsReadOnly());
            SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/*+xml").CopyAsReadOnly());

        }

        public override Task<InputFormatterResult> ReadRequestBodyAsync(InputFormatterContext context, Encoding encoding)
        {
            var request = context.HttpContext.Request;
            XDocument xdoc = null;
            using (var stringReader = new StreamReader(context.HttpContext.Request.Body, encoding, true))
            {
                xdoc = XDocument.Load(stringReader);
            }
            return InputFormatterResult.SuccessAsync(xdoc);
        }
    }
}
