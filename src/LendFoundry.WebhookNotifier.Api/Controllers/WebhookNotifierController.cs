﻿using LendFoundry.Foundation.Services;
using System;
using System.Threading.Tasks;
using System.Xml.Linq;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif


namespace LendFoundry.WebhookNotifier.Api.Controllers
{
    /// <summary>
    /// WebhookNotifierController class
    /// </summary>
    /// <seealso cref="LendFoundry.Foundation.Services.ExtendedController" />
    [Route("/")]
    public class WebhookNotifierController : ExtendedController
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="WebhookNotifierController"/> class.
        /// </summary>
        /// <param name="webhookListener">The webhook listener.</param>
        /// <exception cref="ArgumentNullException">webhookListener</exception>
        public WebhookNotifierController(IWebhookListener webhookListener)
        {
            if (webhookListener == null)
                throw new ArgumentNullException(nameof(webhookListener));

            WebhookListener = webhookListener;
        }

        #endregion Constructor

        #region Private Variables

        private IWebhookListener WebhookListener { get; }

        #endregion Private Variables              

        #region General Request Processing

        /// <summary>
        /// Processes the specified webhooktype.
        /// </summary>
        /// <param name="webhooktype">The webhooktype.</param>
        /// <param name="token">The token.</param>
        /// <param name="data">The data.</param>
        /// <returns></returns>
        [HttpPost("{webhooktype}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Process(string webhooktype, string token, [FromBody] object data)
        {
            return await ExecuteAsync(async () =>
            {
                if(data is XDocument)
                {
                    await WebhookListener.Process(token, webhooktype, data as XDocument);
                }
                else
                {
                    await WebhookListener.Process(token, webhooktype, data);

                }
                return Ok();
            });
        }

        #endregion
    }
}