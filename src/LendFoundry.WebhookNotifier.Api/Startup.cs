﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Encryption;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.WebhookNotifier.Api.Controllers;
using System.Runtime;
using System;
using LendFoundry.Foundation.ServiceDependencyResolver;
using System.Collections.Generic;

#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif

namespace LendFoundry.WebhookNotifier.Api
{
    /// <summary>
    /// Startup class
    /// </summary>
    internal class Startup
    {
        /// <summary>
        /// Configures the services.
        /// </summary>
        /// <param name="services">The services.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            // Register the Swagger generator, defining one or more Swagger documents
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "Webhook Notifier"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme() 
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> 
                { 
					{ "Bearer", new string[]{} }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.WebhookNotifier.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddEncryptionHandler();
            services.AddTenantService();
            services.AddCors();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddCors();
            services.AddEventHub(Settings.ServiceName);
            services.AddConfigurationService<WebhookNotifierConfiguration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<WebhookNotifierConfiguration>(Settings.ServiceName);
            
            services.AddTransient<IWebhookListener,WebhookListener>();
            services.AddTransient<IWebhookNotifierClientService, WebhookNotifierService>();
            services.AddTransient<IWebhookNotifierServiceFactory, WebhookNotifierServiceFactory>();
            services.AddMvc(option=> {
                option.InputFormatters.Add(new CustomInputFormatter());
            }).AddLendFoundryJsonOptions();
        }

        /// <summary>
        /// Configures the specified application.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <param name="env">The env.</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);

            // Enable middleware to serve generated Swagger as a JSON endpoint.
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Webhook Notifier");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseConfigurationCacheDependency();
        }
    }
}