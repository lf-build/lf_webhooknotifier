﻿using LendFoundry.Security.Encryption;
using LendFoundry.Security.Tokens;
using System;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LendFoundry.WebhookNotifier
{
    public class WebhookListener : IWebhookListener
    {
        public WebhookListener(IWebhookNotifierServiceFactory webhookNotifierServiceFactory, ITokenHandler tokenHandler, IEncryptionService encrypterService)
        {
            if (webhookNotifierServiceFactory == null)
                throw new ArgumentNullException(nameof(webhookNotifierServiceFactory));
            if (tokenHandler == null)
                throw new ArgumentNullException(nameof(TokenHandler));
            if (encrypterService == null)
                throw new ArgumentNullException(nameof(EncrypterService));
            WebhookNotifierServiceFactory = webhookNotifierServiceFactory;
            TokenHandler = tokenHandler;
            EncrypterService = encrypterService;
        }
        private IWebhookNotifierServiceFactory WebhookNotifierServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        IEncryptionService EncrypterService { get; }
        public async Task Process(string token, string webhooktype, object data)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentNullException(nameof(token));
            var tenantId = ParseToken(token);
            if (string.IsNullOrWhiteSpace(tenantId))
                throw new ArgumentNullException(nameof(tenantId));

            var tenantToken = TokenHandler.Issue(tenantId, Settings.ServiceName, null, "system", null);
            var reader = new StaticTokenReader(tenantToken.Value);
            var webhookService = WebhookNotifierServiceFactory.Create(reader);
            await webhookService.Process(webhooktype, data);
        }

        public async Task Process(string token, string webhooktype, XDocument request)
        {
            if (string.IsNullOrWhiteSpace(token))
                throw new ArgumentNullException(nameof(token));
            var tenantId = ParseToken(token);
            if (string.IsNullOrWhiteSpace(tenantId))
                throw new ArgumentNullException(nameof(tenantId));
            var tenantToken = TokenHandler.Issue(tenantId, Settings.ServiceName, null, "system", null);
            var reader = new StaticTokenReader(tenantToken.Value);
            var webhookService = WebhookNotifierServiceFactory.Create(reader);
            await webhookService.Process(webhooktype, request);
        }
        private string ParseToken(string token)
        {
            var parsedToken = EncrypterService.Decrypt<string>(token);
            return parsedToken;
        }
    }
}
