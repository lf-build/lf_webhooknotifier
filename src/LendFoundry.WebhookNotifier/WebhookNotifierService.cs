﻿using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LendFoundry.WebhookNotifier
{
    public class WebhookNotifierService : IWebhookNotifierClientService
    {
        #region Constructor

        public WebhookNotifierService(IEventHubClient eventHub, WebhookNotifierConfiguration webhookNotifierConfiguration,ILogger logger,ITenantTime tenantTime)
        {
            if (eventHub == null)
                throw new ArgumentNullException(nameof(eventHub));
            if (webhookNotifierConfiguration == null)
                throw new ArgumentNullException(nameof(webhookNotifierConfiguration));
            EventHub = eventHub;
            WebhookNotifierConfiguration = webhookNotifierConfiguration;
            Logger = logger;
            TenantTime = tenantTime;
        }

        #endregion Constructor

        #region Private Variables    
        private WebhookNotifierConfiguration WebhookNotifierConfiguration { get; set; }
        private IEventHubClient EventHub { get; set; }
        ILogger Logger { get; set; }
        ITenantTime TenantTime { get; set; }
        #endregion Private Variables                     

        #region Process Request
        public async Task Process(string webhooktype, object data)
        {
            if (string.IsNullOrWhiteSpace(webhooktype))
                throw new ArgumentNullException(nameof(webhooktype));

            if (data == null)
                throw new ArgumentNullException(nameof(data));
            try
            {
                Logger.Info("Started Execution for WebhookNotifier Process Request Info: Date & Time:" + TenantTime.Now +"TypeOf object data");

                var eventName = WebhookNotifierConfiguration.WebHooks[webhooktype];
                if (!string.IsNullOrWhiteSpace(eventName))
                {
                    await EventHub.Publish(eventName, new
                    {
                        Response = data,
                        Name = eventName,
                        referencenumber = Guid.NewGuid().ToString("N")
                    });
                }
            }catch(Exception exception)
            {
                Logger.Error("Error While Processing WebhookNotifier Process Date & Time:" + TenantTime.Now + "Exception" + exception.Message);
            }
        }
        public async Task Process(string webhooktype, XDocument xmlrequest)
        {
            if (webhooktype == null)
                throw new ArgumentNullException(nameof(webhooktype));
            try
            {
                Logger.Info("Started Execution for WebhookNotifier Process Request Info: Date & Time:" + TenantTime.Now + "TypeOf xml data");
                var eventName = WebhookNotifierConfiguration.WebHooks[webhooktype];
                if (!string.IsNullOrWhiteSpace(eventName))
                {
                    await EventHub.Publish(eventName, new
                    {
                        Response = xmlrequest,
                        Name = eventName,
                        referencenumber = Guid.NewGuid().ToString("N")
                    });
                }
                Logger.Info("Completed Execution for WebhootNotifier Process Request for webhook type : " + webhooktype );
            }catch(Exception exception)
            {
                Logger.Error("Error While Processing WebhookNotifier Process Date & Time:" + TenantTime.Now +  " Exception" + exception.Message);
            }
        }
        #endregion

    }
}