﻿using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace LendFoundry.WebhookNotifier
{
    public class WebhookNotifierServiceFactory : IWebhookNotifierServiceFactory
    {
        public WebhookNotifierServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }
        private IServiceProvider Provider { get; }

        public IWebhookNotifierClientService Create(ITokenReader reader)
        {
            var eventHubServiceFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHubService = eventHubServiceFactory.Create(reader);

            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var webhokConfigurationService = configurationServiceFactory.Create<WebhookNotifierConfiguration>(Settings.ServiceName, reader);
            var webhookNotifierConfiguration = webhokConfigurationService.Get();
            var loggerFactory = Provider.GetService<ILoggerFactory>();
            var logger = loggerFactory.Create(NullLogContext.Instance);
            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            return new WebhookNotifierService(eventHubService, webhookNotifierConfiguration, logger, tenantTime);
        }
    }
}
