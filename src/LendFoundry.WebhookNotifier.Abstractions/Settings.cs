﻿using System;

namespace LendFoundry.WebhookNotifier
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "webhook-notifier";
    }
}