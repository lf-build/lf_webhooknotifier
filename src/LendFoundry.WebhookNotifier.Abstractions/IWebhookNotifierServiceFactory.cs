﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.WebhookNotifier
{
    public interface IWebhookNotifierServiceFactory
    {
        IWebhookNotifierClientService Create(ITokenReader reader);
    }
}
