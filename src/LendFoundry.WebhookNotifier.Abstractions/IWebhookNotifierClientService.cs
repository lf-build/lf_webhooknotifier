﻿using System.Threading.Tasks;
using System.Xml.Linq;

namespace LendFoundry.WebhookNotifier
{
    public interface IWebhookNotifierClientService
    {       
        Task Process(string webhooktype, object data);
        Task Process(string webhooktype, XDocument request);
    }
}