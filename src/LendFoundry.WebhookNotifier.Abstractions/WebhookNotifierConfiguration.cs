﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.WebhookNotifier
{
    public class WebhookNotifierConfiguration : IDependencyConfiguration
    {
        public Dictionary<string,string> WebHooks { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}