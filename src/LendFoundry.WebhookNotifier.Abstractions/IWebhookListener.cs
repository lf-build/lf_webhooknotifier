﻿using System.Threading.Tasks;
using System.Xml.Linq;

namespace LendFoundry.WebhookNotifier
{
    public interface IWebhookListener
    {
        Task Process(string token, string webhooktype, object data);
        Task Process(string token, string webhooktype, XDocument request);
    }
}
