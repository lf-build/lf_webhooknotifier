﻿using System;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;

#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.WebhookNotifier.Client
{
    public class WebhookNotifierClientServiceFactory : IWebhookNotifierClientServiceFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public WebhookNotifierClientServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public WebhookNotifierClientServiceFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; set; }
        private Uri Uri { get; }

        public IWebhookNotifierClientService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("webhook_notifier");
            }

            var client = Provider.GetServiceClient(reader, uri);
            return new WebhookNotifierClientService(client);
        }
    }
}
