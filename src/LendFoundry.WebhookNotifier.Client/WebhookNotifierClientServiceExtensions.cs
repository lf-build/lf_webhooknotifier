﻿using LendFoundry.Security.Tokens;
using System;

#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif


namespace LendFoundry.WebhookNotifier.Client
{
    public static class WebhookNotifierClientServiceExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddWebhookNotifierService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IWebhookNotifierClientServiceFactory>(p => new WebhookNotifierClientServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IWebhookNotifierClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddWebhookNotifierService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IWebhookNotifierClientServiceFactory>(p => new WebhookNotifierClientServiceFactory(p, uri));
            services.AddTransient(p => p.GetService<IWebhookNotifierClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddWebhookNotifierService(this IServiceCollection services)
        {
            services.AddTransient<IWebhookNotifierClientServiceFactory>(p => new WebhookNotifierClientServiceFactory(p));
            services.AddTransient(p => p.GetService<IWebhookNotifierClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
