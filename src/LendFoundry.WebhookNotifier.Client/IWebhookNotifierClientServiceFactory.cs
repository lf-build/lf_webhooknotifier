﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.WebhookNotifier.Client
{
    public interface IWebhookNotifierClientServiceFactory
    {
        IWebhookNotifierClientService Create(ITokenReader reader);
    }
}
