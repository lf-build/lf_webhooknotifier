﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Threading.Tasks;
using System;
using System.Xml.Linq;

namespace LendFoundry.WebhookNotifier.Client
{
    public class WebhookNotifierClientService : IWebhookNotifierClientService
    {
        public WebhookNotifierClientService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }    
     
        public Task Process(string webhooktype, object data)
        {
            throw new NotImplementedException();
        }

        public Task Process(string webhooktype, XDocument request)
        {
            throw new NotImplementedException();
        }
    }
}